#
# Be sure to run `pod lib lint FDAlertView.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'FDAlertView'
  s.version          = '0.2.4'
  s.summary          = 'FDAlertView is a library that provides a custom alert view'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
FDAlertView is a library that provides a custom alert view.
Some parameters can be edited, like alert corner radius, or padding.
                       DESC

  s.homepage         = 'https://gitlab.com/fredericdnd/FDAlertView.git'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'frederic.dinand.developer@gmail.com' => 'frederic.dinand.developer@gmail.com' }
  s.source           = { :git => 'https://gitlab.com/fredericdnd/FDAlertView.git', :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.ios.deployment_target = '9.0'

  s.source_files = 'FDAlertView/Classes/**/*'
  
  # s.resource_bundles = {
  #   'FDAlertView' => ['FDAlertView/Assets/*.png']
  # }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
  # s.dependency 'AFNetworking', '~> 2.3'
end
