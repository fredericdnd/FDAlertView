# FDAlertView

[![CI Status](https://img.shields.io/travis/frederic.dinand.developer@gmail.com/FDAlertView.svg?style=flat)](https://travis-ci.org/frederic.dinand.developer@gmail.com/FDAlertView)
[![Version](https://img.shields.io/cocoapods/v/FDAlertView.svg?style=flat)](https://cocoapods.org/pods/FDAlertView)
[![License](https://img.shields.io/cocoapods/l/FDAlertView.svg?style=flat)](https://cocoapods.org/pods/FDAlertView)
[![Platform](https://img.shields.io/cocoapods/p/FDAlertView.svg?style=flat)](https://cocoapods.org/pods/FDAlertView)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

FDAlertView is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'FDAlertView'
```

## Author

frederic.dinand.developer@gmail.com, frederic.dinand.developer@gmail.com

## License

FDAlertView is available under the MIT license. See the LICENSE file for more info.
