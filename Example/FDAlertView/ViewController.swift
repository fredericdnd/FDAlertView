import UIKit
import FDAlertView

class ViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func showAlert(_ sender: Any) {
        let alertView = FDAlertView(message: "Title")
        
        let action = FDAlertAction(title: "Action", style: .default) { [unowned self] in
            self.doSomething()
        }
        
        alertView.addAction(action)
        alertView.actionsAreUppercased = false
        alertView.actionsLetterSpacing = 0.0
        alertView.defaultBackgroundColor = UIColor.red
        alertView.buttonsFont = UIFont.boldSystemFont(ofSize: 20.0)
        alertView.addAction(FDAlertAction(title: "Cancel", style: .cancel, handler: nil))
        alertView.show()
    }
    
    func doSomething() {
        print("Something has been done")
    }
    
}
