import UIKit

extension FDAlertView {
    
    public func show() {
        configure()
        
        self.alpha = 0.0
        self.center = UIApplication.shared.keyWindow!.center
        self.containerView.center = self.center
        self.containerView.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
        UIApplication.shared.keyWindow!.addSubview(self)
        
        if animated {
            UIView.animate(withDuration: 0.33, animations: {
                self.alpha = 1.0
            })
            UIView.animate(withDuration: 0.5, delay: 0.0, usingSpringWithDamping: 0.4, initialSpringVelocity: 0.0, options: [.curveEaseInOut], animations: {
                self.containerView.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
            })
        } else {
            self.alpha = 1.0
            self.containerView.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        }
    }
    
    public func hide(completionHandler: (() -> Void)?) {
        if animated {
            UIView.animate(withDuration: 0.1, animations: {
                self.containerView.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
                self.alpha = 0.0
            }, completion: { (completed) in
                self.removeFromSuperview()
                completionHandler?()
            })
        } else {
            self.removeFromSuperview()
            completionHandler?()
        }
    }
    
}
