import UIKit

public enum FDAlertActionStyle {
    
    case cancel
    case `default`
    case destructive
    case positive
    
}

open class FDAlertAction: UIButton {
    
    private var handler: (() -> Void)?
    private var title: String!
    
    public var actionsAreUppercased = true {
        didSet {
            if actionsAreUppercased {
                setTitle(title.uppercased(), for: .normal)
            } else {
                setTitle(title, for: .normal)
            }
        }
    }
    public var letterSpacing: Double = 3.0 {
        didSet {
            titleLabel?.setLetterSpacing(value: letterSpacing)
        }
    }
    public var cancelTitleColor = UIColor(red: 218/255, green: 218/255, blue: 218/255, alpha: 1.0) {
        didSet {
            setUpColors()
        }
    }
    public var defaultBackgroundColor = UIColor(red: 74/255, green: 112/255, blue: 251/255, alpha: 1.0) {
        didSet {
            setUpColors()
        }
    }
    public var destructiveBackgroundColor = UIColor(red: 234/255, green: 61/255, blue: 61/255, alpha: 1.0) {
        didSet {
            setUpColors()
        }
    }
    public var positiveBackgroundColor = UIColor(red: 80/255, green: 227/255, blue: 194/255, alpha: 1.0) {
        didSet {
            setUpColors()
        }
    }
    
    open weak var delegate: FDAlertViewDelegate?
    open var style: FDAlertActionStyle!
    open var cornerRadius: CGFloat = 8.0
    open var buttonFont: UIFont = UIFont.systemFont(ofSize: 17.0, weight: .heavy) {
        didSet {
             titleLabel?.font = buttonFont
        }
    }
    
    public convenience init(title: String, style: FDAlertActionStyle, handler: (() -> Void)?) {
        self.init()
        self.title = title
        self.style = style
        self.configure(handler: handler)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setUp()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        setUp()
    }
    
    private func setUp() {
        self.addTarget(self, action: #selector(buttonTouchDown(_:)), for: .touchDown)
        self.addTarget(self, action: #selector(buttonTouchUpInside(_:)), for: .touchUpInside)
        self.addTarget(self, action: #selector(buttonTouchUp(_:)), for: .touchUpOutside)
        self.addTarget(self, action: #selector(buttonTouchUp(_:)), for: .touchDragExit)
        self.addTarget(self, action: #selector(buttonTouchUp(_:)), for: .touchCancel)
    }
    
    @objc func buttonTouchDown(_ sender: UIButton) {
        UIView.animate(withDuration: 0.1) {
            sender.titleLabel?.alpha = 0.4
        }
    }
    
    @objc func buttonTouchUp(_ sender: UIButton) {
        UIView.animate(withDuration: 0.1) {
            sender.titleLabel?.alpha = 1.0
        }
    }
    
    @objc func buttonTouchUpInside(_ sender: UIButton) {
        UIView.animate(withDuration: 0.1) {
            sender.titleLabel?.alpha = 1.0
        }
        
        DispatchQueue.main.async {
            self.delegate?.hide(completionHandler: { [unowned self] in
                self.handler?()
            })
        }
    }
    
    private func setUpColors() {
        switch style {
        case .cancel?:
            backgroundColor = UIColor.clear
            setTitleColor(cancelTitleColor, for: .normal)
        case .default?:
            backgroundColor = defaultBackgroundColor
            setTitleColor(UIColor.white, for: .normal)
        case .destructive?:
            backgroundColor = destructiveBackgroundColor
            setTitleColor(UIColor.white, for: .normal)
        case .positive?:
            backgroundColor = positiveBackgroundColor
            setTitleColor(UIColor.white, for: .normal)
        default:
            break
        }
    }
    
    private func configure(handler: (() -> Void)?) {
        setUpColors()
        layer.cornerRadius = cornerRadius
        
        self.handler = handler
    }
    
    
    
}
