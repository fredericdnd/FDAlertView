import UIKit

open class FDAlertView: UIView, FDAlertViewDelegate {
    
    private var title: String?
    private var message: String?
    
    open var actions: [FDAlertAction] = []
    open var animated: Bool = true
    open var tapOutsideToDismiss: Bool = true
    open var cornerRadius: CGFloat = 8.0
    open var alertWidth: CGFloat = 343.0
    open var actionHeight: CGFloat = 50.0
    open var padding: CGFloat = 16.0
    open var textColor: UIColor!
    open var actionsAreUppercased = true
    open var actionsLetterSpacing: Double = 3.0
    open var cancelTitleColor = UIColor(red: 218/255, green: 218/255, blue: 218/255, alpha: 1.0)
    open var defaultBackgroundColor = UIColor(red: 74/255, green: 112/255, blue: 251/255, alpha: 1.0)
    open var destructiveBackgroundColor = UIColor(red: 234/255, green: 61/255, blue: 61/255, alpha: 1.0)
    open var positiveBackgroundColor = UIColor(red: 80/255, green: 227/255, blue: 194/255, alpha: 1.0)
    open var titleFont: UIFont = UIFont.systemFont(ofSize: 17.0, weight: .semibold)
    open var messageFont: UIFont = UIFont.systemFont(ofSize: 16.0, weight: .medium)
    open var buttonsFont: UIFont = UIFont.systemFont(ofSize: 17.0, weight: .heavy)
    
    @IBOutlet weak var backgroundView: UIView!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var stackView: UIStackView!
    
    public convenience init(title: String? = nil, message: String? = nil) {
        self.init(frame: UIScreen.main.bounds)
        
        self.title = title
        self.message = message
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupView()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        setupView()
    }
    
    private func loadViewFromNib() -> UIView {
        let viewBundle = Bundle(for: type(of: self))
        let view = viewBundle.loadNibNamed(String(describing: type(of: self)), owner: self, options: nil)![0]
        
        return view as! UIView
    }
    
    private func setupView() {
        let nibView = loadViewFromNib()
        nibView.frame = bounds
        nibView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        addSubview(nibView)
        
        backgroundView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.hide(_:))))
        
        if #available(iOS 13.0, *) {
            containerView.backgroundColor = traitCollection.userInterfaceStyle == .unspecified || traitCollection.userInterfaceStyle == .light ? .white : UIColor(red: 26/255, green: 26/255, blue: 26/255, alpha: 1.0)
            textColor = traitCollection.userInterfaceStyle == .unspecified || traitCollection.userInterfaceStyle == .light ? UIColor(red: 48/255, green: 48/255, blue: 48/255, alpha: 1.0) : .white
        } else {
            containerView.backgroundColor = .white
        }
        
        containerView.layer.cornerRadius = cornerRadius
        stackView.spacing = padding
        stackView.translatesAutoresizingMaskIntoConstraints = false
    }
    
    @objc private func hide(_ sender: Any) {
        guard tapOutsideToDismiss else {
            return
        }
        
        self.hide(completionHandler: nil)
    }
    
    open func addAction(_ action: FDAlertAction) {
        self.actions.append(action)
    }
    
    internal func configure() {
        if let title = title {
            addTitleToAlert(title: title)
        }
        
        if let message = message {
            addMessageToAlert(message: message)
        }
        
        for action in actions {
            addActionToAlert(action)
        }
    }
    
    private func addTitleToAlert(title: String) {
        let titleLabel = UILabel()
        titleLabel.text = title
        titleLabel.textAlignment = .center
        titleLabel.textColor = textColor
        titleLabel.numberOfLines = 0
        titleLabel.font = titleFont
        titleLabel.heightAnchor.constraint(greaterThanOrEqualToConstant: 0.0).isActive = true
        stackView.addArrangedSubview(titleLabel)
        
        titleLabel.layoutIfNeeded()
    }
    
    private func addMessageToAlert(message: String) {
        let messageLabel = UILabel()
        messageLabel.text = message
        messageLabel.textAlignment = .center
        messageLabel.textColor = textColor
        messageLabel.numberOfLines = 0
        messageLabel.font = messageFont
        messageLabel.heightAnchor.constraint(greaterThanOrEqualToConstant: 0.0).isActive = true
        stackView.addArrangedSubview(messageLabel)
        
        messageLabel.layoutIfNeeded()
    }
    
    private func addActionToAlert(_ action: FDAlertAction) {
        switch action.style {
        case .cancel?:
            action.heightAnchor.constraint(equalToConstant: 60.0 - 32.0).isActive = true
            action.backgroundColor = UIColor.clear
        default:
            action.heightAnchor.constraint(equalToConstant: actionHeight).isActive = true
        }
        
        action.actionsAreUppercased = self.actionsAreUppercased
        action.letterSpacing = self.actionsLetterSpacing
        action.cancelTitleColor = self.cancelTitleColor
        action.defaultBackgroundColor = self.defaultBackgroundColor
        action.destructiveBackgroundColor = self.destructiveBackgroundColor
        action.positiveBackgroundColor = self.positiveBackgroundColor
        action.buttonFont = self.buttonsFont
        action.delegate = self
        stackView.addArrangedSubview(action)
    }
    
}

public protocol FDAlertViewDelegate: class {
    
    func hide(completionHandler: (() -> Void)?)
    
}
